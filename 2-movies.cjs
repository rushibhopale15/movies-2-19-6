const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/


//Q1. Find all the movies with total earnings more than $500M.
console.log("Find all the movies with total earnings more than $500M.");
function findTotalEarnings(favouritesMovies) {
    let totalEarningsGreaterThan500 = Object.keys(favouritesMovies).reduce((store, movieName) => {
        let totalEarning = favouritesMovies[movieName].totalEarnings;
        totalEarning = totalEarning.replace(/[^\w.-]/g, '');
        //return totalEarning;
        if (parseInt(totalEarning) > 500 && totalEarning.includes('M')) {
            store[movieName] = favouritesMovies[movieName];
        }
        return store;
    }, {});
    return totalEarningsGreaterThan500;
}
let totalEarningsGreaterThan500 = findTotalEarnings(favouritesMovies);
console.log(totalEarningsGreaterThan500);


//    Q2. Find all the movies who got more than 3 oscarNominations 
// and also totalEarning are more than $500M.
console.log("Q2. Find all the movies who got more than 3 oscarNominations");
function findMoreThan3Oscars(favouritesMovies) {
    let moreThan3Oscars = Object.keys(favouritesMovies).reduce((store, movieName) => {
        let totalEarning = favouritesMovies[movieName].totalEarnings;
        totalEarning = totalEarning.replace(/[^\w.-]/g, '');
        let moreThan3Oscars = favouritesMovies[movieName].oscarNominations;
        if (parseInt(totalEarning) > 500 && totalEarning.includes('M') && moreThan3Oscars > 3) {
            store[movieName] = favouritesMovies[movieName];
        }
        return store;
    }, {});
    return moreThan3Oscars;
}
let moreThan3Oscars = findMoreThan3Oscars(favouritesMovies);
console.log(moreThan3Oscars);


//Q.3 Find all movies of the actor "Leonardo Dicaprio".
console.log("Q.3 Find all movies of the actor Leonardo Dicaprio.");
function findActorLeonardo(favouritesMovies, actorName) {
    let actorLeonardo = Object.keys(favouritesMovies).filter((movieName)=>{
        let movieActors = favouritesMovies[movieName].actors;
        return movieActors.includes(actorName);
    })
    //console.log(actorLeonardo);
    return actorLeonardo;
}
let actorLeonardo = findActorLeonardo(favouritesMovies, "Leonardo Dicaprio");
console.log(actorLeonardo);


//Q.4 Sort movies (based on IMDB rating)
//if IMDB ratings are same, compare totalEarning as the secondary metric.
console.log("Q.4 Sort movies (based on IMDB rating)");
function sortMovies(favouritesMovies){

    let sortedMovie = Object.keys(favouritesMovies).sort((movie1, movie2) => {
        let imdbRatingMovie1 = favouritesMovies[movie1].imdbRating;
        let imdbRatingMovie2 = favouritesMovies[movie2].imdbRating;
        if(imdbRatingMovie1 == imdbRatingMovie2){
            let totalEarningMovie1 = favouritesMovies[movie1].totalEarnings;
            totalEarningMovie1 = totalEarningMovie1.replace(/[^\w.-]/g, '');
            let totalEarningMovie2 = favouritesMovies[movie2].totalEarnings;
            totalEarningMovie2 = totalEarningMovie2.replace(/[^\w.-]/g, '');
            
            return parseInt(totalEarningMovie1) - parseInt(totalEarningMovie2);
        }
        return imdbRatingMovie1 - imdbRatingMovie2;
    })
    return sortedMovie;
}
let sortedMovie = sortMovies(favouritesMovies);
let sortedMovieData = sortedMovie.map((movieName)=>{
    return { movieName ,value : favouritesMovies[movieName] };
})
console.log(sortedMovieData);


//Q.5 Group movies based on genre. Priority of genres 
//in case of multiple genres present are:
//drama > sci-fi > adventure > thriller > crime
console.log("Group movies based on genre. Priority of genres ");
function makeGroupBasedOnGenre(favouritesMovies) {
    let genres = {'drama':[], 'sci-fi':[], 'adventure':[], 'thriller':[],'crime': []};
    let groupBasedOnGenre = Object.keys(favouritesMovies)
    .map((movieName) => {
        let movieGenre = favouritesMovies[movieName].genre;
        let genrewithMovie = movieGenre.map((genre)=>{
            if(genre in genres){
                let movies = genres[genre];
                movies.push(movieName);
                genres[genre] = movies;
            }
            return genre;
        });
        return movieName;
    })
    //console.log(genres);
    return genres;
}
console.log(makeGroupBasedOnGenre(favouritesMovies));
